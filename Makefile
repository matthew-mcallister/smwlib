all: lint

.PHONY: fmt
fmt:
	autopep8 -aa --recursive --in-place .
	autoflake --recursive --in-place .
	PYTHONPATH=. removestar --in-place .

.PHONY: lint
lint: fmt
	flake8
