from dataclasses import dataclass


@dataclass
class SliceIO:
    view: memoryview
    # Enables seek() support
    offset: int

    def __init__(self, data: bytes):
        self.view = memoryview(data)
        self.offset = 0

    def read(self, size: int) -> memoryview:
        res = self.view[self.offset:self.offset + size]
        if len(res) < size:
            raise EOFError
        self.offset += size
        return res

    def read_u8(self) -> int:
        return self.read(1)[0]

    def peek(self, size: int) -> memoryview:
        return self.view[self.offset:self.offset + size]

    def peek_u8(self) -> int:
        try:
            return self.view[self.offset]
        except IndexError:
            raise EOFError

    def write(self, data: bytes):
        if len(data) > len(self.view) - self.offset:
            raise EOFError
        self.view[self.offset:self.offset + len(data)] = data
        self.offset += len(data)

    def write_u8(self, val: int):
        self.write(bytes(val))
