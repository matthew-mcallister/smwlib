from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from typing import Iterator, List, Union

from .obj import Object, ObjectKind, ScreenExit
from .raw import RawObject, RawPrimaryHeader, RawSecondaryHeader
from .raw import decode_rle


_SCREEN_WIDTH = 2**4
_SCREEN_HEIGHT = 2**5


class Layer2Kind(Enum):
    OBJECTS = 0
    BACKGROUND = 1

    @staticmethod
    def from_ptr(ptr: int) -> Layer2Kind:
        is_bg = (ptr & 0xFF0000) == 0xFF0000
        return Layer2Kind(int(is_bg))


@dataclass
class Level:
    primary_header: RawPrimaryHeader
    secondary_header: RawSecondaryHeader
    screen_exits: List[ScreenExit]
    layer1: List[Object]
    layer2: Union[List[Object], List[int]]
    layer2_kind: Layer2Kind

    @staticmethod
    def decode(
        layer1: bytes,
        layer2: bytes,
        layer2_kind: Layer2Kind,
        secondary_header: RawSecondaryHeader,
        sprites: bytes,
    ) -> Level:
        # Decode layer 1
        primary_header, raw_objs = RawObject.decode_stream(layer1)
        layer1_objs = []
        screen_exits = []
        for obj in Level._decode_objects(raw_objs):
            if obj.kind == ObjectKind.SCREEN_EXIT:
                screen_exits.append(obj)
            else:
                layer1_objs.append(obj)

        # Decode layer 2
        if layer2_kind == Layer2Kind.OBJECTS:
            _, raw_objs = RawObject.decode_stream(layer2)
            layer2_data = []
            for obj in Level._decode_objects(raw_objs):
                # TODO: Unsure if screen exits can appear in layer 2 data
                if obj.kind != ObjectKind.SCREEN_EXIT:
                    layer2_data.append(obj)
        else:
            layer2_data = decode_rle(layer2)

        # TODO: Decode sprites

        return Level(
            primary_header,
            secondary_header,
            screen_exits,
            layer1_objs,
            layer2_data,
            layer2_kind,
        )

    @staticmethod
    def _decode_objects(raw_objs: List[RawObject]) -> (
        Iterator[Union[Object, ScreenExit]]
    ):
        screen = 0
        for raw in raw_objs:
            kind = ObjectKind.from_raw(raw)
            if kind == ObjectKind.SCREEN_EXIT:
                yield ScreenExit.decode(raw)
                continue
            elif kind == ObjectKind.SCREEN_JUMP:
                screen += raw.y
                continue

            if raw.new_screen:
                screen += 1

            # I believe this calculation is correct assuming the origin
            # is at the lower-left
            # TODO: Vertical levels!
            x = screen * _SCREEN_WIDTH + raw.x
            y = _SCREEN_HEIGHT - raw.y

            settings = raw.decode_settings()
            yield Object(
                kind,
                x, y,
                settings.get('height'),
                settings.get('width'),
            )
