from __future__ import annotations

from dataclasses import dataclass
from typing import Optional

from .raw import RawObject


@dataclass
class ObjectKind:
    """The numeric ID that uniquely identifies a particular in-game
    object type.

    Vanilla objects map into the ID space in a rather convoluted way:
    the base object ID is treated as the high byte, while the low byte
    is determined from the object settings byte.
    """
    value: int

    @staticmethod
    def from_raw(obj: RawObject) -> ObjectKind:
        hi = int(obj.kind) << 8
        try:
            settings = obj.decode_settings()
            return ObjectKind(hi | settings['variant'])
        except KeyError:
            return ObjectKind(hi)

    def __repr__(self) -> str:
        return f'ObjectKind(0x{self.value:x})'

    def __int__(self) -> int:
        return self.value


# These are invalid object kinds and are currently reserved.
ObjectKind.SCREEN_EXIT = ObjectKind(0x0000)
ObjectKind.SCREEN_JUMP = ObjectKind(0x0100)


@dataclass
class Object:
    """An object, such as a block or coin.

    Special objects (i.e. screen exits and screen jumps) are considered
    artifacts of the level encoding and are not represented by the main
    Object class.

    The object's position is given in absolute coordinates starting from
    the lower left, not screen-relative coordinates.
    """
    kind: ObjectKind
    x: int
    y: int
    height: Optional[int] = None
    width: Optional[int] = None


@dataclass
class ScreenExit:
    """Assigns the destination to any exits (e.g. pipes) on a screen.

    The destination is either a secondary exit ID or level number.
    """
    screen_num: int
    secondary_exit: bool
    destination: int

    @staticmethod
    def decode(raw: RawObject) -> ScreenExit:
        screen_num = raw.y
        secondary_exit = bool(raw.x & 0b10)
        destination = raw.ex_settings
        assert destination is not None
        # For some reason, bit 0 of raw.x is never used.
        return ScreenExit(screen_num, secondary_exit, destination)

    @property
    def kind(self) -> ObjectKind:
        return ObjectKind.SCREEN_EXIT
