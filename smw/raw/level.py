from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from typing import List

from ..io import SliceIO


class LevelOrientation(Enum):
    HORIZONTAL = 0
    VERTICAL = 1


def _starts_with(prefix, s):
    return s[:len(prefix)] == prefix


class RawLevelMode(Enum):
    HORIZONTAL = 0x00
    HORIZONTAL_LAYER2_PASSABLE = 0x01
    HORIZONTAL_LAYER2 = 0x02
    # Supposedly these two modes glitch when player spawns on screen 0D
    VERTICAL_LAYER2_PASSABLE = 0x07
    VERTICAL_LAYER2 = 0x08
    HORIZONTAL_BOSS_1 = 0x09
    VERTICAL = 0x0A
    HORIZONTAL_BOSS_2 = 0x0B
    HORIZONTAL_LAYER2_DARKBG = 0x0C
    VERTICAL_LAYER2_DARKBG = 0x0C
    HORIZONTAL_2 = 0x0E
    HORIZONTAL_LAYER2_PASSABLE_2 = 0x0F
    HORIZONTAL_BOSS_3 = 0x10
    HORIZONTAL_LAYER2_DARK = 0x11
    HORIZONTAL_TRANSLUCENT = 0x1E
    HORIZONTAL_LAYER2_TRANSLUCENT = 0x1F

    @property
    def orientation(self):
        if _starts_with('HORIZONTAL', self.name):
            return LevelOrientation.HORIZONTAL
        else:
            assert _starts_with('VERTICAL', self.name)
            return LevelOrientation.VERTICAL


class RawPlayerStartAction(Enum):
    NONE = 0
    EXIT_PIPE_LEFTWARD = 1
    EXIT_PIPE_RIGHTWARD = 2
    EXIT_PIPE_UPWARD = 3
    EXIT_PIPE_DOWNWARD = 4
    SLIPPERY = 5
    EXIT_CANNON = 6
    EXIT_PIPE_UNDERWATER = 7


@dataclass
class RawPrimaryHeader:
    # TODO: Figure out meaning of each field
    bg_palette: int
    num_screens: int
    back_color: int
    mode: RawLevelMode
    layer3_priority: bool
    music: int
    sprite_gfx: int
    timer: int
    sprite_palette: int
    fg_palette: int
    item_mem: int
    vertical_scroll: int
    object_gfx: int

    @staticmethod
    def decode(enc: bytes) -> RawPrimaryHeader:
        return RawPrimaryHeader(
            bg_palette=enc[0] >> 5,
            num_screens=enc[0] & 0b1_1111,

            back_color=enc[1] >> 5,
            mode=RawLevelMode(enc[1] & 0b1_1111),

            layer3_priority=bool(enc[2] >> 7),
            music=enc[2] >> 4 & 0b111,
            sprite_gfx=enc[2] & 0b1111,

            timer=enc[3] >> 6,
            sprite_palette=enc[3] >> 3 & 0b111,
            fg_palette=enc[3] & 0b111,

            item_mem=enc[4] >> 6,
            vertical_scroll=enc[4] >> 4 & 0b11,
            object_gfx=enc[4] & 0b1111,
        )


@dataclass
class RawSecondaryHeader:
    # x is 3 bytes, y is 4
    player_start_x: int
    player_start_y: int
    player_start_action: RawPlayerStartAction
    player_start_screen: int
    player_start_on_upper_screen: bool
    midway_start_screen: int
    # Offsets are vertical for horiz. lvls and horiz. for vertical lvls
    fg_offset: int
    bg_offset: int
    # TODO: Need meaning of these fields
    layer2_scroll: int
    layer3_mode: int
    no_yoshi: bool

    @staticmethod
    def decode(*args) -> RawSecondaryHeader:
        b0: int
        b1: int
        b2: int
        b3: int
        if len(args) == 1:
            assert len(args[0]) == 4
            b0, b1, b2, b3 = args[0]
        else:
            assert len(args) == 4
            b0, b1, b2, b3 = args

        return RawSecondaryHeader(
            layer2_scroll=b0 >> 4,
            player_start_y=b0 & 0b1111,

            layer3_mode=b1 >> 6,
            player_start_action=RawPlayerStartAction(b1 >> 3 & 0b111),
            player_start_x=b1 & 0b111,

            midway_start_screen=b2 >> 4,
            fg_offset=b2 >> 2 & 0b11,
            bg_offset=b2 & 0b11,

            no_yoshi=bool(b3 >> 7),
            player_start_on_upper_screen=bool(b3 >> 5 & 1),
            player_start_screen=b3 & 0b1_1111,
        )


def decode_rle(rle: bytes) -> List[int]:
    # TODO: Maybe belongs in a `utils` module
    stream = SliceIO(rle)
    bs = []
    while True:
        cmd = stream.read_u8()

        if cmd == 0xFF and stream.peek_u8() == 0xFF:
            return bs

        is_run = cmd & 0x80
        count = (cmd & 0x7F) + 1
        if is_run:
            byte = stream.read_u8()
            bs.extend(byte for _ in range(count))
        else:
            bs.extend(stream.read(count))
