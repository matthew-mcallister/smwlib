from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from typing import Dict, Iterator, Optional

from ..io import SliceIO
from .level import RawPrimaryHeader


class RawObjectKind(Enum):
    EXTENDED = 0x00, ['variant']
    WATER_BLUE = 0x01, ['height', 'width']
    HIDDEN_BLOCK_COIN = 0x02, ['height', 'width']
    HIDDEN_NOTE_BLOCK = 0x03, ['height', 'width']
    COIN_PSWITCH = 0x04, ['height', 'width']
    COIN = 0x05, ['height', 'width']
    PASSABLE_TERRAIN = 0x06, ['height', 'width']
    WATER_COLORED = 0x07, ['height', 'width']
    NOTE_BLOCK = 0x08, ['height', 'width']
    TURN_BLOCK = 0x09, ['height', 'width']
    QUESTION_BLOCK_COIN = 0x0A, ['height', 'width']
    THROW_BLOCK = 0x0B, ['height', 'width']
    MUNCHER = 0x0C, ['height', 'width']
    CEMENT = 0x0D, ['height', 'width']
    USED_BLOCK = 0x0E, ['height', 'width']
    PIPE_VERTICAL = 0x0F, ['height', 'variant']
    PIPE_HORIZONTAL = 0x10, ['variant', 'width']
    BULLET_SHOOTER = 0x11, ['height', None]
    SLOPE = 0x12, ['height', 'variant']
    TERRAIN_EDGE = 0x13, ['height', 'variant']
    TERRAIN_FLOOR = 0x14, ['height', 'width']
    GOAL = 0x15, ['height', 'variant']
    BLUE_COIN = 0x16, ['height', 'width']
    ROPE_OR_CLOUD = 0x17, ['variant', 'width']
    WATER_SURFACE_ANIM = 0x18, ['height', 'width']
    WATER_SURFACE_STATIC = 0x19, ['height', 'width']
    LAVA_SURFACE_ANIM = 0x1A, ['height', 'width']
    FENCE_TOP_EDGE = 0x1B, ['height', 'width']
    DONUT_BRIDGE = 0x1C, [None, 'width']
    FENCE_BOTTOM_EDGE = 0x1D, ['height', 'width']
    FENCE_VERTICAL_EDGE = 0x1E, ['height', 'variant']
    BEAM_VERTICAL = 0x1F, ['height', None]
    BEAM_HORIZONTAL = 0x20, [None, 'width']
    TERRAIN_FLOOR_LONG = 0x21, ['width']
    LM_MAP16_PAGE0 = 0x22, []
    LM_MAP16_PAGE1 = 0x23, []
    LM_GFX_BYPASS = 0x24, []
    LM_AN2_BYPASS = 0x25, []
    LM_MUSIC_BYPASS = 0x26, []
    LM_MAP16_OBJECT = 0x27, []
    LM_TIME_LIMIT_BYPASS = 0x28, []
    LM_RESERVED_0 = 0x29, []
    LM_RESERVED_1 = 0x2A, []
    LM_RESERVED_2 = 0x2B, []
    LM_RESERVED_3 = 0x2C, []
    LM_RESERVED_4 = 0x2D, []
    TILESET_1 = 0x2E, []
    TILESET_2 = 0x2F, []
    TILESET_3 = 0x30, []
    TILESET_4 = 0x31, []
    TILESET_5 = 0x32, []
    TILESET_6 = 0x33, []
    TILESET_7 = 0x34, []
    TILESET_8 = 0x35, []
    TILESET_9 = 0x36, []
    TILESET_10 = 0x37, []
    TILESET_11 = 0x38, []
    TILESET_12 = 0x39, []
    TILESET_13 = 0x3A, []
    TILESET_14 = 0x3B, []
    TILESET_15 = 0x3C, []
    TILESET_16 = 0x3D, []
    TILESET_17 = 0x3E, []
    TILESET_18 = 0x3F, []

    def __new__(cls, value, props):
        obj = object.__new__(cls)
        obj._value_ = value
        obj.props = props
        return obj

    def __int__(self):
        return self._value_


class ExObjectKind(Enum):
    SCREEN_EXIT = 0x00
    SCREEN_JUMP = 0x01
    SMALL_DOOR = 0x10
    HIDDEN_BLOCK_1UP = 0x11
    HIDDEN_NOTE_BLOCK = 0x12
    UL_CORNER_1 = 0x13
    UR_CORNER_1 = 0x14
    SMALL_DOOR_PSWITCH = 0x15
    HIDDEN_BLOCK_PSWITCH = 0x16
    GREEN_STAR_BLOCK = 0x17
    MOON = 0x18
    HIDDEN_1UP_1 = 0x19
    HIDDEN_1UP_2 = 0x1A
    HIDDEN_1UP_3 = 0x1B
    HIDDEN_1UP_4 = 0x1C
    BERRY_RED = 0x1D
    BERRY_PINK = 0x1E
    BERRY_GREEN = 0x1F
    SPINNING_BLOCK = 0x20
    MIDWAY_LR = 0x21
    MIDWAY_LL = 0x22
    NOTE_BLOCK_POWERUP = 0x23
    ON_OFF_BLOCK = 0x24
    SNAKE_COIN_BLOCK = 0x25
    NOTE_BLOCK = 0x26
    NOTE_BLOCK_ALL_SIDES_BOUNCY = 0x27
    TURN_BLOCK_FLOWER = 0x28
    TURN_BLOCK_FEATHER = 0x29
    TURN_BLOCK_STAR = 0x2A
    TURN_BLOCK_ETC = 0x2B
    TURN_BLOCK_MULTIPLE_COINS = 0x2C
    TURN_BLOCK_COIN = 0x2D
    TURN_BLOCK_NOTHING = 0x2E
    TURN_BLOCK_PSWITCH = 0x2F
    QUESTION_BLOCK_FLOWER = 0x30
    QUESTION_BLOCK_FEATHER = 0x31
    QUESTION_BLOCK_STAR = 0x32
    QUESTION_BLOCK_STAR_2 = 0x33
    QUESTION_BLOCK_MULTIPLE_COINS = 0x34
    QUESTION_BLOCK_ETC = 0x35
    QUESTION_BLOCK_YOSHI = 0x36
    QUESTION_BLOCK_SHELL = 0x37
    QUESTION_BLOCK_SHELL_2 = 0x38
    TURN_BLOCK_UNBREAKABLE_FEATHER = 0x39
    UL_CORNER_2 = 0x3A
    UR_CORNER_2 = 0x3B
    UL_CORNER_3 = 0x3C
    UR_CORNER_3 = 0x3D
    UL_CORNER_4 = 0x3E
    UR_CORNER_4 = 0x3F
    TRANSCULENT_BLOCK = 0x40
    YOSHI_COIN = 0x41
    SLOPE_UL = 0x42
    SLOPE_UR = 0x43
    RAMP_L = 0x44
    RAMP_R = 0x45
    MIDWAY_TAPE = 0x46
    DOOR = 0x47
    DOOR_PSWITCH = 0x48
    GHOST_HOUSE_EXIT = 0x49
    FENCE_DOOR = 0x4A
    CONVEYOR_END_1 = 0x4B
    CONVEYOR_END_2 = 0x4C
    TRACK_UL_QUARTER_CIRCLE_LARGE = 0x4D
    TRACK_UR_QUARTER_CIRCLE_LARGE = 0x4E
    TRACK_LL_QUARTER_CIRCLE_LARGE = 0x4F
    TRACK_LR_QUARTER_CIRCLE_LARGE = 0x50
    TRACK_UL_QUARTER_CIRCLE_SMALL = 0x51
    TRACK_UR_QUARTER_CIRCLE_SMALL = 0x52
    TRACK_LL_QUARTER_CIRCLE_SMALL = 0x53
    TRACK_LR_QUARTER_CIRCLE_SMALL = 0x54
    TRACK_END_HORIZONTAL = 0x55
    TRACK_END_VERTICAL = 0x56
    PALACE_LR = 0x57
    PALACE_LL = 0x58
    PALACE_UR = 0x59
    PALACE_UL = 0x5A
    BRICK1 = 0x5B
    BRICK2 = 0x5C
    BRICK3 = 0x5D
    BRICK4 = 0x5E
    BACKGROUND = 0x5F
    LAVA_UR_EDGE = 0x60
    CLOCK = 0x61
    BEAM_UL_TO_LR_1 = 0x62
    BEAM_UR_TO_LL_1 = 0x63
    COBWEB_UR = 0x64
    COBWEB_UL = 0x65
    BEAM_UR_TO_LL_2 = 0x66
    BEAM_UL_TO_LR_2 = 0x67
    CLOUD_BOTTOM_PLUS_RIGHT_EDGE = 0x68
    CLOUD_BOTTOM_PLUS_LEFT_EDGE = 0x69
    CLOUD_LR = 0x6A
    CLOUD_LL = 0x6B
    CLOUD_BOTTOM_PLUS_RIGHT_EDGE_WHITE = 0x6C
    CLOUD_BOTTOM_PLUS_LEFT_EDGE_WHITE = 0x6D
    CLOUD_LR_WHITE = 0x6E
    CLOUD_LL_WHITE = 0x6F
    CANVAS_FRAGMENT_1 = 0x70
    CANVAS_1 = 0x71
    CANVAS_2 = 0x72
    CANVAS_3 = 0x73
    CANVAS_4 = 0x74
    CANVAS_TILE_1 = 0x75
    CANVAS_TILE_2 = 0x76
    CANVAS_TILE_3 = 0x77
    CANVAS_TILE_4 = 0x78
    CANVAS_TILE_5 = 0x79
    CANVAS_TILE_6 = 0x7A
    CANVAS_TILE_7 = 0x7B
    CANVAS_FRAGMENT_2 = 0x7C
    CANVAS_FRAGMENT_3 = 0x7D
    CANVAS_FRAGMENT_4 = 0x7E
    TORPEDO_LAUNCHER = 0x7F
    GHOST_HOUSE_ENTRANCE = 0x80
    SEAWEED = 0x81
    BUSH_1 = 0x82
    BUSH_2 = 0x83
    CASTLE_ENTRANCE = 0x84
    YOSHI_HOUSE = 0x85
    ARROW_SIGN = 0x86
    SWITCH_BLOCK_GREEN = 0x87
    TREE_BRANCH_L = 0x88
    TREE_BRANCH_R = 0x89
    SWITCH_GREEN = 0x8A
    SWITCH_YELLOW = 0x8B
    SWITCH_BLUE = 0x8C
    SWITCH_RED = 0x8D
    SWITCH_BLOCK_YELLOW = 0x8E
    GHOST_HOUSE_WINDOW = 0x8F
    BOSS_DOOR = 0x90
    STEEP_SLOPE_VERTICAL_L = 0x91
    STEEP_SLOPE_VERTICAL_R = 0x92
    NORMAL_SLOPE_VERTICAL_L = 0x93
    NORMAL_SLOPE_VERTICAL_R = 0x94
    VERY_STEEP_SLOPE_VERTICAL_L = 0x95
    VERY_STEEP_SLOPE_VERTICAL_R = 0x96
    PALACE_BOTTOM_PLUS_RIGHT_EDGE = 0x97


@dataclass
class RawObject:
    kind: RawObjectKind
    x: int
    y: int
    new_screen: bool
    settings: int
    ex_settings: Optional[int] = None

    @staticmethod
    def decode(stream: SliceIO) -> RawObject:
        enc = stream.read(3)

        assert enc[0] != 0xFF, 'object cannot start with array end marker'

        ns = bool(enc[0] >> 7)

        B = enc[0] >> 5 & 0b11
        b = enc[1] >> 4 & 0b1111
        kind = RawObjectKind(B << 4 | b)

        x = enc[1] & 0b1111
        y = enc[0] & 0b11111

        s = enc[2]

        s2 = None
        if kind == RawObjectKind.EXTENDED and s == 0:
            # Screen exits consume an extra byte
            s2 = stream.read_u8()

        return RawObject(kind, x, y, ns, s, s2)

    def encode(self) -> bytes:
        ns = int(self.new_screen)

        kind = int(self.kind)
        B = kind >> 4
        b = kind & 0xF

        b0 = ns << 7 | B << 5 | self.y
        b1 = b << 4 | self.x

        assert b0 != 0xFF, 'object cannot start with array end marker'

        if self.ex_settings is not None:
            return bytes([b0, b1, self.settings, self.ex_settings])
        else:
            return bytes([b0, b1, self.settings])

    def decode_settings(self) -> Dict[str, int]:
        """Decodes extra properties based on the object kind."""
        # TODO: tileset-specific settings
        props = self.kind.props
        assert len(props) <= 2

        if not props:
            settings = {}
        elif len(props) == 1:
            settings = {props[0]: self.settings}
        else:
            settings = {
                props[0]: self.settings >> 4,
                props[1]: self.settings & 0x0F,
            }

        try:
            del settings[None]
        except KeyError:
            pass

        return settings

    @staticmethod
    def decode_stream(data: bytes) -> (RawPrimaryHeader, Iterator[RawObject]):
        header = RawPrimaryHeader.decode(data[:5])
        objs = RawObject._decode_stream_objs(data[5:])
        return header, objs

    @staticmethod
    def _decode_stream_objs(data: bytes) -> Iterator[RawObject]:
        stream = SliceIO(data)
        while stream.peek_u8() != 0xFF:
            yield RawObject.decode(stream)
