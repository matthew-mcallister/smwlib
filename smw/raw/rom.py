from typing import Iterator, List, Tuple

from ..level import Layer2Kind, Level
from .level import RawSecondaryHeader


def addr_to_rom(addr: int) -> int:
    """Convert a memory address to the corresponding location in the
    ROM.

    The SMC header is not counted as part of the ROM.
    """
    return (addr >> 16) * 0x8000 | (addr & 0x7FFF)


def rom_to_addr(rom: int):
    """Convert a ROM address to the corresponding location in memory

    The SMC header is not counted as part of the ROM.
    """
    return (rom // 0x8000) << 16 | (rom % 0x8000) | 0x8000


class RawRom:
    LEVEL_COUNT = 0x200
    _SMC_HEADER_LEN = 0x200
    _LAYER1_PTR_TABLE_START = addr_to_rom(0x05E000)
    _LAYER2_PTR_TABLE_START = addr_to_rom(0x05E600)
    _SPRITE_PTR_TABLE_START = addr_to_rom(0x05EC00)
    _SECONDARY_HEADER_TABLE_START = addr_to_rom(0x05F000)

    def __init__(self, data: bytes):
        self.buf = bytearray(data)

        HEADER_LEN = RawRom._SMC_HEADER_LEN
        if len(self.buf) % (2 * HEADER_LEN) == HEADER_LEN:
            # strip SMC header
            self.header = memoryview(self.buf)[:HEADER_LEN]
            self.data = memoryview(self.buf)[HEADER_LEN:]
        else:
            self.header = None
            self.data = memoryview(self.buf)

        self.layer1_ptrs = list(self._read_layer1_ptr_table())
        self.layer2_ptrs = list(self._read_layer2_ptr_table())
        self.secondary_headers = self._read_secondary_headers()

    def _iter_ptrs(self, start: int) -> Iterator[int]:
        table = self.data[start:start + 3 * RawRom.LEVEL_COUNT]
        return (
            int.from_bytes(table[offs:offs + 3], 'little')
            for offs in range(0, RawRom.LEVEL_COUNT * 3, 3)
        )

    def _read_layer1_ptr_table(self) -> Iterator[int]:
        return self._iter_ptrs(RawRom._LAYER1_PTR_TABLE_START)

    def _read_layer2_ptr_table(self) -> Iterator[Tuple[Layer2Kind, int]]:
        for ptr in self._iter_ptrs(RawRom._LAYER2_PTR_TABLE_START):
            kind = Layer2Kind.from_ptr(ptr)
            if kind == Layer2Kind.BACKGROUND:
                # Yes, SMW hardcodes one bank for all level backgrounds
                ptr = (ptr & 0x00FFFF) | 0x0C0000
            yield kind, ptr

    def _read_secondary_headers(self) -> List[RawSecondaryHeader]:
        data = self.data[RawRom._SECONDARY_HEADER_TABLE_START:]
        stride = RawRom.LEVEL_COUNT
        bs = (
            bytes(data[stride * i + level] for i in range(4))
            for level in range(RawRom.LEVEL_COUNT)
        )
        return [RawSecondaryHeader.decode(b) for b in bs]

    def _decode_level(self, level: int) -> Level:
        layer2_kind, layer2_ptr = self.layer2_ptrs[level]
        return Level.decode(
            layer1=self.data[addr_to_rom(self.layer1_ptrs[level]):],
            layer2=self.data[addr_to_rom(layer2_ptr):],
            layer2_kind=layer2_kind,
            secondary_header=self.secondary_headers[level],
            sprites=bytes(),
        )
